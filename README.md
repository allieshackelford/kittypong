# Kitty Pong
Kitty Pong is cat themed version of the classic Pong game created in a custom Engine (provided by James Mansfield) at Vancouver Film School.
This project is in C++ using component based architecture.
## Dependencies
CMake 3.8+, https://cmake.org/download/
## How to Install
Run 'GenerateProject.bat' to create project solution
Open KittyPong.sln and build
## How to Play
Open the game from the download link below.
On the main menu, press 1 or 2 to choose between the options and press Enter to make a selection.
For Player vs AI, use W and S to move the left paddle.
For Player vs Player, use W and S for left paddle and Up arrow Down arrow for right paddle.


# DOWNLOAD 
COMING SOON

## Screenshots

![](/Assets/Images/MainMenu.PNG)

![](Assets/Images/InGame.PNG)



Copyright (C) 2020, Alexandrea Shackelford. All Rights Reserved.